﻿using CustomerOrderManagmentService.Models;
using CustomerOrderManagmentService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerOrderManagmentService.Controllers
{
    [Route("v1/api/[controller]/")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderServices _services;

        public OrdersController (IOrderServices services)
        {
            _services = services;
        }
        /// <summary>
        /// 
        /// This method will create new orders 
        /// 
        /// </summary>
       
      
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<Orders> CreateOrder(Orders item)
        {
            if (item.CustomerId == 0 || item.CustomerId > 3)
                return BadRequest("Customer Id should be 1,2 or 3!");
            else if (item.ProductId == 0 || item.ProductId > 4)
                return BadRequest(" Product Id must be eighter 1,2,3,4!");
            else if (item.Quantity <= 0 )
                return BadRequest("Product Qauntity should be valid value!");
            else
                if(item.PricePaid<=0)
                return BadRequest("PricePaid should be valid positive value!");

            if (ModelState.IsValid)
            {
             Orders NewOrder=   _services.PlaceOrder(item);
                return NewOrder;
               
            }
            else
            {
              return BadRequest(ModelState);
                
            }
            
           

        }
        /// <summary>
        /// 
        /// This method will return all shipment pending orders 
        /// 
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]

        public ActionResult<List<PendingOrders>> GetPendingOrders()
        {
            try { 
          var resultset=  _services.PendingOrders();

            if (resultset.Count > 0 && resultset.Max(a=>a.OrderId)>0)
                return resultset;
            else
                return NotFound();
            }
            catch(Exception ex)
            {
                throw ex;

            }
        }
    }
}