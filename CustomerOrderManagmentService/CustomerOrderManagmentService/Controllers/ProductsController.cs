﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerOrderManagmentService.Models;
using CustomerOrderManagmentService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomerOrderManagmentService.Controllers
{
    [Route("v1/api/[controller]/")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductServices _services;
       
        public ProductsController(IProductServices services)
        {
            _services = services;
        }
        /// <summary>
        /// This method will return all product details
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<List<Products>> GetProductsList()
        {
            var ProductsList = _services.GetProductDetails();
            if (ProductsList.Count==0)
            {
                return NotFound();
            }
            
             return ProductsList;

        }

      
    }
}