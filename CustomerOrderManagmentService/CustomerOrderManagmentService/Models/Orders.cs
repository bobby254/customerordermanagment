﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerOrderManagmentService.Models
{
    public class Orders
    {
     
        public int   OrderId { get; set; }
        [Required(ErrorMessage ="ProductId field is mandatory!")]
     
        public int   ProductId { get; set; }
        [Required(ErrorMessage = "CustomerId field is mandatory!")]
        public int   CustomerId { get; set; }
        public DateTime   OrderDate { get; set; }
        [Required(ErrorMessage ="Please provide the Quantity!")]
        public int   Quantity { get; set; }
        [Required(ErrorMessage ="Please provide Pricepaid for item!")]
        public decimal   PricePaid { get; set; }
        public DateTime   ShippedDate { get; set; }
    }
}
