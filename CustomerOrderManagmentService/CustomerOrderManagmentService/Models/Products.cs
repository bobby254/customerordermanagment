﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerOrderManagmentService.Models
{
    public class Products
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal PricePerItem { get; set; }
        public int AverageCustomerRating { get; set; }
        
    }
}
