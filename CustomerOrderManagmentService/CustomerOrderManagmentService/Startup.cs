﻿using CustomerOrderManagmentService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using GlobalExceptionHandler.WebApi;
using System;
using Newtonsoft.Json;

namespace CustomerOrderManagmentService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSingleton<IProductServices, ProductServices>();
            services.AddSingleton<IOrderServices, OrderServices>();
            services.AddSingleton<ICustomerServices, CustomerServices>();
          
            services.AddSwaggerGen(a =>
            {
                a.SwaggerDoc("v1", new OpenApiInfo { Title = "Cust Order Managment API", Version = "v1" });
                var xmlpath = System.AppDomain.CurrentDomain.BaseDirectory + @"CustomerOrderManagmentService.xml";
                a.IncludeXmlComments(xmlpath);
            });
           
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
              
                app.UseHsts();
            }
            app.UseGlobalExceptionHandler(x => {
                x.ContentType = "application/json";
                x.ResponseBody(s => JsonConvert.SerializeObject(new
                {
                    Message = "An error occurred whilst processing your request"
                }));
            });

            app.Map("/error", x => x.Run(y => throw new Exception()));
            app.UseHttpsRedirection();
            app.UseMvc();
            
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cust Order Managment API");
            });
        }
    }
}
