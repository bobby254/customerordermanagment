﻿using CustomerOrderManagmentService.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerOrderManagmentService.Services
{
    public class OrderServices : IOrderServices
    {
        private readonly List<Orders> _OrderItems;

            public  OrderServices()
        {
            _OrderItems = new List<Orders>();
        }
        public Orders PlaceOrder(Orders Items)
        {
            try
            {
                if (_OrderItems.Count == 0)
                    Items.OrderId = 1;
                else
                    Items.OrderId = _OrderItems.Max(a => a.OrderId) + 1;

                Items.ShippedDate = DateTime.Now.AddDays(1);
                Items.OrderDate = DateTime.Now;

            _OrderItems.Add(Items);

            return Items;
            }
            catch
            {
               return Items;
            }
        }

        public List<PendingOrders> PendingOrders()
        {
            CustomerServices Obj = new CustomerServices();
            
            var CustomerList = from cust in Obj.GetCustomers()
                               join ord in _OrderItems on cust.CustomerId equals ord.CustomerId
                               where ord.ShippedDate> DateTime.Now
                               select new PendingOrders { CustomerId=cust.CustomerId,CustomerName=cust.CustomerName,OrderId=ord.OrderId,OrderDate=ord.OrderDate,PricePaid=ord.PricePaid,Quantity=ord.Quantity} ;

            return CustomerList.ToList();






        }
    }
}
