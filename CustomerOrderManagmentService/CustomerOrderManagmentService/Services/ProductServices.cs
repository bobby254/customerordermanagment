﻿using CustomerOrderManagmentService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerOrderManagmentService.Services
{
    public class ProductServices : IProductServices
    {
        private readonly List<Products> _ProductsList;
        public ProductServices()
        {
            _ProductsList =new List<Products>();
            AddProducts();

        }

        public void AddProducts()
        {
            _ProductsList.Add(new Products { ProductId = 1, ProductName = "Books", PricePerItem = 12.0M, AverageCustomerRating = 4 });
            _ProductsList.Add( new Products { ProductId = 2, ProductName = "pens", PricePerItem = 5.0M, AverageCustomerRating = 3 });
            _ProductsList.Add( new Products { ProductId = 3, ProductName = "Pencils", PricePerItem = 2.0M, AverageCustomerRating = 5 });
            _ProductsList.Add( new Products { ProductId =4, ProductName = "Ink", PricePerItem = 15.0M, AverageCustomerRating = 2 });
           // return _ProductsList.OrderByDescending(a => a.AverageCustomerRating).ToList();
        }
        public List<Products> GetProductDetails()
        {
           
          return _ProductsList.OrderByDescending(a => a.AverageCustomerRating).ToList();
        }

    }
}
