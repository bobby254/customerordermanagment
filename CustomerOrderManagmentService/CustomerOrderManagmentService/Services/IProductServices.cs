﻿using CustomerOrderManagmentService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerOrderManagmentService.Services
{
   public interface IProductServices
    {
         List<Products> GetProductDetails();
    }
}
