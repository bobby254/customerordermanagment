﻿using CustomerOrderManagmentService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerOrderManagmentService.Services
{
    public interface IOrderServices
    {
        Orders PlaceOrder(Orders Items);
        List<PendingOrders> PendingOrders();
    }
}
