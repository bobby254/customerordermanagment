﻿using CustomerOrderManagmentService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerOrderManagmentService.Services
{
    public class CustomerServices : ICustomerServices
    {
        private readonly List<Customers> _services;

        public CustomerServices()
        {
            _services = new List<Customers>();
        }

        public List<Customers> GetCustomers()
        {
            _services.Add(new Customers { CustomerId = 1, CustomerName = "Bobby", AddressLine1 = "7305 Hagen ct", AddressLine2 = "1506", City = "Charlotte", State = "NC", Zip = "28262", Country = "US" });
            _services.Add(new Customers { CustomerId = 2, CustomerName = "Vihaan", AddressLine1 = "1234 NTT ct", AddressLine2 = "123", City = "Concord", State = "NC", Zip = "28262", Country = "US" });
            _services.Add(new Customers { CustomerId = 3, CustomerName = "Satya", AddressLine1 = "65656 IBM ct", AddressLine2 = "567", City = "Oaks", State = "PH", Zip = "28262", Country = "US" });
            return _services;
        }

    }
}
